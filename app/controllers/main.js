var args = $.args;

var Map = require('ti.map');
var mapview;
var locationAdded = false;
var webview;

var uLatitude;
var uLongitude;

var alertAudio = Titanium.Media.createSound({
	url : '/audios/alert.mp3',
	preload : true
});

optsConfig = {cancel : 2,options : ['Cerrar Sesión', 'Cancelar'],selectedIndex : 2,destructive : 0,title : 'Configuración'};
dialogConfig = Titanium.UI.createOptionDialog(optsConfig);

//Menu
function toggleMenu() {
	if ($.menu.menuToggle == true) {
		$.menu.animate({
			left : -360,
			duration : 150,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
		$.menu.menuToggle = false;

		$.shadowView.hide();
		$.shadowView.removeEventListener('click', toggleMenu);

	} else {
		$.menu.animate({
			left : 0,
			duration : 150,
			curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
		$.menu.menuToggle = true;

		$.shadowView.show();
		$.shadowView.addEventListener('click', toggleMenu);
	}
}

/* Begin Geohash */
"use strict";

var geohash = {},
    geo_base32 = "0123456789bcdefghjkmnpqrstuvwxyz",
    geo_base32_map = {
	0 : 0,
	1 : 1,
	2 : 2,
	3 : 3,
	4 : 4,
	5 : 5,
	6 : 6,
	7 : 7,
	8 : 8,
	9 : 9,
	b : 10,
	c : 11,
	d : 12,
	e : 13,
	f : 14,
	g : 15,
	h : 16,
	j : 17,
	k : 18,
	m : 19,
	n : 20,
	p : 21,
	q : 22,
	r : 23,
	s : 24,
	t : 25,
	u : 26,
	v : 27,
	w : 28,
	x : 29,
	y : 30,
	z : 31
};

/**
 * encode_i2c Private function
 * @param  {Number} lat        Latitude
 * @param  {Number} lng        Longitude
 * @param  {Number} lat_length Latitude length
 * @param  {Number} lng_length Longitude length
 * @return {String} Calced string
 */
function encode_i2c(lat, lng, lat_length, lng_length) {
	var a,
	    b,
	    t,
	    i,
	    base32 = geo_base32.split(''),
	    precision = (lat_length + lng_length) / 5,
	    boost = [0, 1, 4, 5, 16, 17, 20, 21],
	    ret = "";
	if (lat_length < lng_length) {
		a = lng;
		b = lat;
	} else {
		a = lat;
		b = lng;
	}
	boost = [0, 1, 4, 5, 16, 17, 20, 21];
	ret = "";

	for ( i = 0; i < precision; i += 1) {
		ret += base32[(boost[a & 7] + (boost[b & 3] << 1)) & 0x1F];
		t = parseInt((a * Math.pow(2, -3)), 10);
		a = parseInt((b * Math.pow(2, -2)), 10);
		b = t;
	}
	return ret.split('').reverse().join('');
}

/**
 * decode_c2i Private function
 * @param  {String} hashcode Calced GeoHash
 * @return {Array} Latitude, Longitude, Latitude length, Longitude length
 */
function decode_c2i(hashcode) {
	var i,
	    t,
	    lng = 0,
	    lat = 0,
	    bit_length = 0,
	    lat_length = 0,
	    lng_length = 0,
	    hash = hashcode.split('');

	for ( i = 0; i < hash.length; i += 1) {
		t = geo_base32_map[hash[i]];
		if (bit_length % 2 === 0) {
			lng = lng * 8;
			lat = lat * 4;
			lng += (t / 4) & 4;
			lat += (t / 4) & 2;
			lng += (t / 2) & 2;
			lat += (t / 2) & 1;
			lng += t & 1;
			lng_length += 3;
			lat_length += 2;
		} else {
			lng = lng * 4;
			lat = lat * 8;
			lat += (t / 4) & 4;
			lng += (t / 4) & 2;
			lat += (t / 2) & 2;
			lng += (t / 2) & 1;
			lat += t & 1;
			lng_length += 2;
			lat_length += 3;
		}
		bit_length += 5;
	}
	return [lat, lng, lat_length, lng_length];
}

/**
 * encode method
 * @param  {Number} lat       Latitude
 * @param  {Number} lng       Longitude
 * @param  {Number} precision Needs precision
 * @return {String} Calced GeoHash
 */
geohash.encode = function(lat, lng, precision) {
	precision = precision || 12;
	if (lat >= 90 || lat <= -90) {
		return "";
	}
	while (lng < -180.0) {
		lng += 360.0;
	}
	while (lng >= 180.0) {
		lng -= 360.0;
	}
	lat = lat / 180.0;
	lng = lng / 360.0;

	var xprecision = precision + 1,
	    lat_length = parseInt((xprecision * 5 / 2), 10),
	    lng_length = parseInt((xprecision * 5 / 2), 10);

	if (xprecision % 2 === 1) {
		lng_length += 1;
	}

	if (lat > 0) {
		lat = parseInt((Math.pow(2, lat_length) * lat + Math.pow(2, lat_length - 1)), 10);
	} else {
		lat = Math.pow(2, lat_length - 1) - parseInt((Math.pow(2, lat_length) * (-1.0 * lat)), 10);
	}

	if (lng > 0) {
		lng = parseInt((Math.pow(2, lng_length) * lng + Math.pow(2, lng_length - 1)), 10);
	} else {
		lng = Math.pow(2, lng_length - 1) - parseInt((Math.pow(2, lng_length) * (-1.0 * lng)), 10);
	}

	return encode_i2c(lat, lng, lat_length, lng_length).substring(0, precision);
};

/**
 * decode method
 * @param  {String}  hashcode Calced GeoHash
 * @param  {Boolean} delta    Delta flag
 * @return {Array} Latitude, Longitude
 */
geohash.decode = function(hashcode, delta) {
	delta = delta || false;
	var data,
	    lat,
	    lng,
	    lat_length,
	    lng_length,
	    latitude,
	    longitude,
	    latitude_delta,
	    longitude_delta;
	data = decode_c2i(hashcode);
	lat = data[0];
	lng = data[1];
	lat_length = data[2];
	lng_length = data[3];

	lat = (lat * 2) + 1;
	lng = (lng * 2) + 1;
	lat_length += 1;
	lng_length += 1;

	latitude = 180.0 * (lat - Math.pow(2, (lat_length - 1))) / Math.pow(2, lat_length);
	longitude = 360.0 * (lng - Math.pow(2, (lng_length - 1))) / Math.pow(2, lng_length);
	if (delta) {
		latitude_delta = 180.0 / Math.pow(2, lat_length);
		longitude_delta = 360.0 / Math.pow(2, lng_length);
		return [latitude, longitude, latitude_delta, longitude_delta];
	}
	return [latitude, longitude];
};

/**
 * decode_exactly method (Enable Delta flag)
 * @param  {String} hashcode Calced GeoHash
 * @return {Array} Latitude, Longitude
 */
geohash.decode_exactly = function(hashcode) {
	return geohash.decode(hashcode, true);
};

/**
 * bbox method
 * @param  {String} hashcode Calced GeoHash
 * @return {Object} Norce, South, East, West
 */
geohash.bbox = function(hashcode) {
	var data = decode_c2i(hashcode),
	    lat = data[0],
	    lng = data[1],
	    lat_length = data[2],
	    lng_length = data[3],
	    ret = {};

	if (lat_length) {
		ret.n = 180.0 * (lat + 1 - Math.pow(2, (lat_length - 1))) / Math.pow(2, lat_length);
		ret.s = 180.0 * (lat - Math.pow(2, (lat_length - 1))) / Math.pow(2, lat_length);
	} else {
		ret.n = 90.0;
		ret.s = -90.0;
	}
	if (lng_length) {
		ret.e = 360.0 * (lng + 1 - Math.pow(2, (lng_length - 1))) / Math.pow(2, lng_length);
		ret.w = 360.0 * (lng - Math.pow(2, (lng_length - 1))) / Math.pow(2, lng_length);
	} else {
		ret.e = 180.0;
		ret.w = -180.0;
	}

	return ret;
};

/**
 * neignbors method
 * @param  {String} hashcode Calced GeoHash
 * @return {Array} Calced near 8 points GeoHash
 */
geohash.neighbors = function(hashcode) {
	var data = decode_c2i(hashcode),
	    lat = data[0],
	    lng = data[1],
	    lat_length = data[2],
	    lng_length = data[3],
	    ret = [],
	    tlat =
	    lat,
	    tlng =
	    lng;

	ret.push(encode_i2c(tlat, tlng - 1, lat_length, lng_length));
	ret.push(encode_i2c(tlat, tlng + 1, lat_length, lng_length));

	tlat = lat + 1;
	if (tlat >= 0) {
		ret.push(encode_i2c(tlat, tlng - 1, lat_length, lng_length));
		ret.push(encode_i2c(tlat, tlng, lat_length, lng_length));
		ret.push(encode_i2c(tlat, tlng + 1, lat_length, lng_length));
	}
	tlat = lat - 1;
	if ((tlat / Math.pow(2, lat_length)) !== 0) {
		ret.push(encode_i2c(tlat, tlng - 1, lat_length, lng_length));
		ret.push(encode_i2c(tlat, tlng, lat_length, lng_length));
		ret.push(encode_i2c(tlat, tlng + 1, lat_length, lng_length));
	}
	return ret;
};

/**
 * expand method (Neighbor GeoHashes with self)
 * @param  {String} hashcode Calced GeoHash
 * @return {Array} Calced near 8 points GeoHash with argument geohash
 */
geohash.expand = function(hashcode) {
	var ret = geohash.neighbors(hashcode);
	ret.push(hashcode);
	return ret;
};

/**
 * contain method
 * @param  {Number}  lat      Latitude
 * @param  {Number}  lng      Longitude
 * @param  {String}  hashcode Calced GeoHash
 * @return {Boolean}
 */
geohash.contain = function(lat, lng, hashcode) {
	var data = geohash.bbox(hashcode);
	if (lat < data.n && lat > data.s && lng > data.w && lng < data.e) {
		return true;
	}
	return false;
};

/**
 * contain_expand method
 * @param  {Number}  lat      Latitude
 * @param  {Number}  lng      Longitude
 * @param  {String}  hashcode Calced GeoHash
 * @return {Boolean}
 */
geohash.contain_expand = function(lat, lng, hashcode) {
	var i,
	    data = geohash.expand(hashcode);
	for ( i = 0; i < data.length; i += 1) {
		if (geohash.contain(lat, lng, data[i])) {
			return true;
		}
	}
	return false;
};

/* End Geohash */
webview = Ti.UI.createWebView({
	url : '/webview/firebase/index.html',
	width : 0,
	height : 0,
	top : 0,
	left : 0,
	visible : false,
});
webview.addEventListener('beforeload', function(e) {
	webview.evalJS("var firebaseApiKey='" + Alloy.CFG.FirebaseApiKey + "'; var firebaseAuthDomain='" + Alloy.CFG.FirebaseAuthDomain + "'; var firebaseDatabaseURL='" + Alloy.CFG.FirebaseDatabaseURL + "';");
});
webview.addEventListener('load', function() {
	Ti.API.info('load firebase webview');
	Ti.App.fireEvent('firebase:init', {
		driverID :Ti.App.Properties.getInt('iddriver'),
		firebaseUser : Alloy.CFG.FirebaseUser,
		firebasePassword : Alloy.CFG.FirebasePassword
	});
});

if (Titanium.App.Properties.hasProperty('driverStatus') == 1) {
	$.status.image = "/images/icon-driver-on.png";
	gpsStart();
}else if (Titanium.App.Properties.hasProperty('driverStatus') == 2){
	$.status.image = "/images/icon-driver-off.png";
	gpsStop();
}

$.container.add(webview);

function toggleDriverStatus(e) {
	var message = '';
	turnbutton = this;
	if (Titanium.App.Properties.hasProperty('driverStatus') == 1) {
		turnbutton.driverStatus = 'on';
	}else if (Titanium.App.Properties.hasProperty('driverStatus') == 2){
		turnbutton.driverStatus = 'off';
	}
	if (this.driverStatus == 'on') {
		message = '¿Desea ponerse inactivo?';
	} else if (this.driverStatus == 'off') {
		message = '¿Desea ponerse activo?';
	}

	var dialog = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Si', 'No'],
		message : message,
		title : 'Activarse'
	});

	dialog.addEventListener('click', function(e) {
		if (e.index === e.source.cancel) {
			Ti.API.info('The cancel button was clicked');
		}
		if (e.index == 0) {
			if (turnbutton.driverStatus == 'on') {
				if (Ti.App.Properties.getInt('isOrderActive') == 1) {
					var error_dialog = Ti.UI.createAlertDialog({
						cancel : 0,
						buttonNames : ['Ok'],
						message : 'You cannot stop gps while your are on a delivery',
						title : 'Error'
					});
					error_dialog.show();

				} else {
					$.status.image = "/images/icon-driver-off.png";
					turnbutton.driverStatus = 'off';
					gpsStop();
					Titanium.App.Properties.setInt('driverStatus',2);
				}
			} else if (turnbutton.driverStatus == 'off') {
				$.status.image = "/images/icon-driver-on.png";
				turnbutton.driverStatus = 'on';
				gpsStart();
				Titanium.App.Properties.setInt('driverStatus',1);
			}
		}
	});
	dialog.show();
}

function gpsStart() {
	if (Ti.Geolocation.locationServicesEnabled) {
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
		Ti.Geolocation.distanceFilter = 10;

		if (!locationAdded) {
			Ti.Geolocation.addEventListener('location', handleLocation);
			locationAdded = true;
		}

	} else {
		alert('Please enable location services');
	}
}

function gpsStop() {
	if (locationAdded) {
		Ti.Geolocation.removeEventListener('location', handleLocation);
		locationAdded = false;

		Ti.App.fireEvent('firebase:innactive');

		Ti.API.info('gps is disable');
	}
}

function handleLocation(e) {
	if (e.error) {
		Ti.API.info('Error: ' + e.error);
	} else if (!e.error) {
		Ti.API.info(e);

		"use strict";

		var gh = geohash.encode(e.coords.latitude, e.coords.longitude, 10);

		Ti.API.info('geohash ' + gh);

		uLatitude = e.coords.latitude;
		uLongitude = e.coords.longitude;

		Ti.App.fireEvent('firebase:updateLocation', {
			gh : gh,
			altitude : e.coords.altitude,
			heading : e.coords.heading,
			latitude : e.coords.latitude,
			longitude : e.coords.longitude,
			speed : e.coords.speed
		});

	}

	Ti.API.info(e);
}

function getGPSCoordinates() {
	if (Ti.Geolocation.locationServicesEnabled) {
		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
		Ti.Geolocation.distanceFilter = 10;
		Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;

		Ti.Geolocation.getCurrentPosition(function(e) {

			Titanium.App.Properties.setBool('locationEnabled', true);

			Ti.API.info(e);

			if (!e.success || e.error) {
				Ti.API.info('error:' + JSON.stringify(e.error));
				return;
			}

			var latitude = e.coords.latitude;
			var longitude = e.coords.longitude;

			mapview = Map.createView({
				mapType : Map.NORMAL_TYPE,
				region : {
					latitude : latitude,
					longitude : longitude,
					latitudeDelta : 0.02,
					longitudeDelta : 0.02,
					userLocation : true,
					visible : true
				},
				animate : false,
				regionFit : true,
				userLocation : true,
			});

			$.container.add(mapview);

		});

	} else {
		alert('Please enable location services');
		mapview = Map.createView({
			mapType : Map.NORMAL_TYPE,
			region : {
				latitudeDelta : 0.02,
				longitudeDelta : 0.02,
				userLocation : true,
				visible : true
			},
			animate : true,
			regionFit : true,
			userLocation : true,
		});

		$.container.add(mapview);
	}
}

function signOff() {
	Titanium.App.Properties.removeProperty('iduser');
	Titanium.App.Properties.removeProperty('iddriver');
	Titanium.App.Properties.removeProperty('name');
	Titanium.App.Properties.removeProperty('lastname');
	Titanium.App.Properties.removeProperty('phone');
	Titanium.App.Properties.removeProperty('email');
	var index = Alloy.createController("index").getView();
	if (OS_IOS) {
		index.open();
	} else if (OS_ANDROID) {
		index.open();
	}
};

function open() {
	if (Ti.Geolocation.locationServicesEnabled) {
		getGPSCoordinates();
	} else {
		Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(e) {
			getGPSCoordinates();
		});
	}
	$.menuButton.addEventListener('click', toggleMenu);
	$.statusButton.addEventListener('click', toggleDriverStatus);
	$.main.addEventListener('close', close);
}

function close() {
	$.menuButton.removeEventListener('click', toggleMenu);
	$.main.removeEventListener('open', open);
	$.main.removeEventListener('close', close);
}

$.exit.addEventListener('click', function() {
	dialogConfig.show();
});

dialogConfig.addEventListener('click', function(e) {
	if (e.index == 0) {
		signOff();
	}
});

Ti.App.addEventListener('main:openInvitation', function(e) {
	var snapshot = e.snapshot;
	
	Ti.API.info(snapshot.client_id);

	//Alloy.Globals.showInvitation = 1;

	var message = '¿Desea aceptar esta invitación?';

	var dialog = Ti.UI.createAlertDialog({
		cancel : 1,
		buttonNames : ['Si', 'No'],
		message : message,
		title : 'Invitación'
	});

	dialog.addEventListener('click', function(e) {
		if (e.index === e.source.cancel) {
			alertAudio.stop();

			//Alloy.Globals.showInvitation = 0;

			Ti.App.fireEvent('firebase:declineInvitation', {
				client_id : snapshot.client_id
			});
		}
		if (e.index == 0) {
			alertAudio.stop();

			Alloy.Globals.showInvitation = 0;
			$.btnStatusServices.visible = true;
			Ti.App.fireEvent('firebase:acceptInvitation', {
				client_id : snapshot.client_id
			});
		}
	});
	dialog.show();

	alertAudio.play();

});

Ti.App.addEventListener('invitation:accept', function(e) {

});

Ti.App.addEventListener('invitation:decline', function(e) {

});

$.main.addEventListener('open', open); 