var args = $.args;

//Webservices
urlLogin = Alloy.CFG.APIurl + 'logindriver';
xhrLogin = Ti.Network.createHTTPClient({timeout : 5000});
dialogMessage = Titanium.UI.createAlertDialog({buttonNames : ['Aceptar'],title : ''});

$.email.addEventListener('return', function() {
	$.password.focus();
});

function login() {
	if ($.email.value != '' || $.password.value != '') {
		params = {
			apikey : Alloy.CFG.APIkey,
			user : $.email.value,
			password : $.password.value
		};
		xhrLogin.open("POST", urlLogin);
		xhrLogin.send(params);
	} else {
		dialogMessage.title = 'Error';
		dialogMessage.message = 'Todos los campos son obligatorios.';
		dialogMessage.show();
	}
};
xhrLogin.onload = function() {
	response = JSON.parse(this.responseText);
	if (response.status == "success") {
		Ti.App.Properties.setInt("iduser", response.data.iduser);
		Ti.App.Properties.setInt("iddriver", response.data.iddriver);
		Ti.App.Properties.setString("name", response.data.name);
		Ti.App.Properties.setString("lastname", response.data.lastname);
		Ti.App.Properties.setString("phone", response.data.phone);
		Ti.App.Properties.setString("email", response.data.email);
		openMain();
	} else {
		dialogMessage.title = 'Error';
		dialogMessage.message = response.message;
		dialogMessage.show();
	}
};
xhrLogin.onerror = function(e) {
	dialogMessage.title = 'Error';
	dialogMessage.message = 'Verifica tu conexión a internet';
	dialogMessage.show();
};
function openMain() {
	var openMain = Alloy.createController("main").getView();
	if (OS_IOS) {
		openMain.open();
	} else if (OS_ANDROID) {
		openMain.open();
	}
}
function open() {
	$.signInEmailButton.addEventListener('click', login);
	$.index.addEventListener('close', close);
}
function close() {
	$.index.removeEventListener('open', open);
	$.index.removeEventListener('close', close);
}
if (Ti.App.Properties.hasProperty('iduser')) {
	openMain();
}else{
	$.index.addEventListener('open', open);
	Alloy.Globals.index = $.index;
	Alloy.Globals.index.open();
}